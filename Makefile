CC=gcc
FLAGS= -std=c11 -Wall 
OBJ=prog.o get_table.o sort_table.o merge.o print_table.o 
INCL=.
LIB=-lc
INSDIR=/usr/bin



myexe: 	$(OBJ)
	$(CC) $(OBJ) $(LIB) -o myexe
prog.o: prog.c
	$(CC) -c $(FLAGS) -I$(INCL) prog.c 
get_table.o: get_table.c
	$(CC) -c $(FLAGS)  get_table.c
sort_table.o: sort_table.c
	$(CC) -c $(FLAGS) sort_table.c
merg.o: merg.c
	$(CC) -c $(FLAGS) merge.c
print_table: print_table.c
	$(CC) -c $(FLAGS) print_table.c
clean:
	@rm -f $(OBJ)
	@rm -f myexe
install:
	@chmod +x myexe
	@chmod og-w myexe
	@cp myexe $(INSDIR)/myexe
	@echo "myexe has been installed successfully!!"
uninstall:
	@rm $(INSDIR)/myexe
	@echo " myexe has been uninstall successfully!!"
