#ifndef ME_HDR
#define ME_HDR
#include<stdio.h>
#include<stdlib.h>
int get_table(int *, int);
void sort_table(int *, int);
void print_table(int*, int);
void merge(int *, int);
#endif
